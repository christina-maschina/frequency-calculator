package android.tvz.hr.kalkulatorvidakovic;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.tvz.hr.kalkulatorvidakovic.databinding.ActivityNoteBinding;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.slider.Slider;

public class NoteActivity extends AppCompatActivity {
    RadioGroup radioGroup1;
    RadioButton radioButton;
    String selectedNote;
    private ActivityNoteBinding binding;
    Slider slider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding=ActivityNoteBinding.inflate(getLayoutInflater());
        View view=binding.getRoot();
        setContentView(view);

        //radioGroup1 = findViewById(R.id.radioGroup1);
        //Button buttonNext = findViewById(R.id.button_next);
        binding.buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedID1 = binding.radioGroup.getCheckedRadioButtonId();
                radioButton = findViewById(selectedID1);

                selectedNote = radioButton.getText().toString();
                Intent intent = new Intent(NoteActivity.this, OctaveActivity.class);
                intent.putExtra("note", selectedNote);
                startActivity(intent);
            }
        });
    }

}