package android.tvz.hr.kalkulatorvidakovic;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.tvz.hr.kalkulatorvidakovic.databinding.ActivityOctaveBinding;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.AbstractMap;
import java.util.Map;

@RequiresApi(api = Build.VERSION_CODES.R)
public class OctaveActivity extends AppCompatActivity {
    private RadioButton radioButton;
    private String selectedOctave;
    private String selectedNote;
    private Integer n = null;
    private ActivityOctaveBinding binding;

    /*private static final Map<String, Integer> DATA = Map.ofEntries(
            new AbstractMap.SimpleEntry<String, Integer>("ASub Contra", 1),
            new AbstractMap.SimpleEntry<String, Integer>("A#/BbSub Contra", 2),
            new AbstractMap.SimpleEntry<String, Integer>("BSub Contra", 3),
            new AbstractMap.SimpleEntry<String, Integer>("CContra", 4),
            new AbstractMap.SimpleEntry<String, Integer>("C#/DbContra", 5),
            new AbstractMap.SimpleEntry<String, Integer>("DContra", 6),
            new AbstractMap.SimpleEntry<String, Integer>("D#/EbContra", 7),
            new AbstractMap.SimpleEntry<String, Integer>("EContra", 8),
            new AbstractMap.SimpleEntry<String, Integer>("FContra", 9),
            new AbstractMap.SimpleEntry<String, Integer>("F#/GbContra", 10),
            new AbstractMap.SimpleEntry<String, Integer>("GContra", 11),
            new AbstractMap.SimpleEntry<String, Integer>("G#/AbContra", 12),
            new AbstractMap.SimpleEntry<String, Integer>("AContra", 13),
            new AbstractMap.SimpleEntry<String, Integer>("A#/BbContra", 14),
            new AbstractMap.SimpleEntry<String, Integer>("BContra", 15),
            new AbstractMap.SimpleEntry<String, Integer>("CGreat", 16),
            new AbstractMap.SimpleEntry<String, Integer>("C#/DbGreat", 17),
            new AbstractMap.SimpleEntry<String, Integer>("DGreat", 18),
            new AbstractMap.SimpleEntry<String, Integer>("D#/EbGreat", 19),
            new AbstractMap.SimpleEntry<String, Integer>("EGreat", 20),
            new AbstractMap.SimpleEntry<String, Integer>("FGreat", 21),
            new AbstractMap.SimpleEntry<String, Integer>("F#/GbGreat", 22),
            new AbstractMap.SimpleEntry<String, Integer>("GGreat", 23),
            new AbstractMap.SimpleEntry<String, Integer>("G#/AbGreat", 24),
            new AbstractMap.SimpleEntry<String, Integer>("AGreat", 25),
            new AbstractMap.SimpleEntry<String, Integer>("A#/BbGreat", 26),
            new AbstractMap.SimpleEntry<String, Integer>("BGreat", 27),
            new AbstractMap.SimpleEntry<String, Integer>("CSmall", 28),
            new AbstractMap.SimpleEntry<String, Integer>("C#/DbSmall", 29),
            new AbstractMap.SimpleEntry<String, Integer>("DSmall", 30),
            new AbstractMap.SimpleEntry<String, Integer>("D#/EbSmall", 31),
            new AbstractMap.SimpleEntry<String, Integer>("ESmall", 32),
            new AbstractMap.SimpleEntry<String, Integer>("FSmall", 33),
            new AbstractMap.SimpleEntry<String, Integer>("F#/GbSmall", 34),
            new AbstractMap.SimpleEntry<String, Integer>("GSmall", 35),
            new AbstractMap.SimpleEntry<String, Integer>("G#/AbSmall", 36),
            new AbstractMap.SimpleEntry<String, Integer>("ASmall", 37),
            new AbstractMap.SimpleEntry<String, Integer>("A#/BbSmall", 38),
            new AbstractMap.SimpleEntry<String, Integer>("BSmall", 39),
            new AbstractMap.SimpleEntry<String, Integer>("C1-line", 40),
            new AbstractMap.SimpleEntry<String, Integer>("C#/Db1-line", 41),
            new AbstractMap.SimpleEntry<String, Integer>("D1-line", 42),
            new AbstractMap.SimpleEntry<String, Integer>("D#/Eb1-line", 43),
            new AbstractMap.SimpleEntry<String, Integer>("E1-line", 44),
            new AbstractMap.SimpleEntry<String, Integer>("F1-line", 45),
            new AbstractMap.SimpleEntry<String, Integer>("F#/Gb1-line", 46),
            new AbstractMap.SimpleEntry<String, Integer>("G1-line", 47),
            new AbstractMap.SimpleEntry<String, Integer>("G#/Ab1-line", 48),
            new AbstractMap.SimpleEntry<String, Integer>("A1-line", 49),
            new AbstractMap.SimpleEntry<String, Integer>("A#/Bb1-line", 50),
            new AbstractMap.SimpleEntry<String, Integer>("B1-line", 51),
            new AbstractMap.SimpleEntry<String, Integer>("C2", 52),
            new AbstractMap.SimpleEntry<String, Integer>("C#/Db2", 53),
            new AbstractMap.SimpleEntry<String, Integer>("D2", 54),
            new AbstractMap.SimpleEntry<String, Integer>("D#/Eb2", 55),
            new AbstractMap.SimpleEntry<String, Integer>("E2", 56),
            new AbstractMap.SimpleEntry<String, Integer>("F2", 57),
            new AbstractMap.SimpleEntry<String, Integer>("F#/Gb2", 58),
            new AbstractMap.SimpleEntry<String, Integer>("G2", 59),
            new AbstractMap.SimpleEntry<String, Integer>("G#/Ab2", 60),
            new AbstractMap.SimpleEntry<String, Integer>("A2", 61),
            new AbstractMap.SimpleEntry<String, Integer>("A#/Bb2", 62),
            new AbstractMap.SimpleEntry<String, Integer>("B2", 63),
            new AbstractMap.SimpleEntry<String, Integer>("C3", 64),
            new AbstractMap.SimpleEntry<String, Integer>("C#/Db3", 65),
            new AbstractMap.SimpleEntry<String, Integer>("D3", 66),
            new AbstractMap.SimpleEntry<String, Integer>("D#/Eb3", 67),
            new AbstractMap.SimpleEntry<String, Integer>("E3", 68),
            new AbstractMap.SimpleEntry<String, Integer>("F3", 69),
            new AbstractMap.SimpleEntry<String, Integer>("F#/Gb3", 70),
            new AbstractMap.SimpleEntry<String, Integer>("G3", 71),
            new AbstractMap.SimpleEntry<String, Integer>("G#/Ab3", 72),
            new AbstractMap.SimpleEntry<String, Integer>("A3", 73),
            new AbstractMap.SimpleEntry<String, Integer>("A#/Bb3", 74),
            new AbstractMap.SimpleEntry<String, Integer>("B3", 75),
            new AbstractMap.SimpleEntry<String, Integer>("C4", 76),
            new AbstractMap.SimpleEntry<String, Integer>("C#/Db4", 77),
            new AbstractMap.SimpleEntry<String, Integer>("D4", 78),
            new AbstractMap.SimpleEntry<String, Integer>("D#/Eb4", 79),
            new AbstractMap.SimpleEntry<String, Integer>("E4", 80),
            new AbstractMap.SimpleEntry<String, Integer>("F4", 81),
            new AbstractMap.SimpleEntry<String, Integer>("F#/Gb4", 82),
            new AbstractMap.SimpleEntry<String, Integer>("G4", 83),
            new AbstractMap.SimpleEntry<String, Integer>("G#/Ab4", 84),
            new AbstractMap.SimpleEntry<String, Integer>("A4", 85),
            new AbstractMap.SimpleEntry<String, Integer>("A#/Bb4", 86),
            new AbstractMap.SimpleEntry<String, Integer>("B4", 87));*/
    Map<String, Integer> mapOfNotes;
    Map<String, Integer> mapOfOctaves;
    Map<String, Integer> mapForSpecialCase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("OCT", "I'm in octave activity");

        super.onCreate(savedInstanceState);
        binding=ActivityOctaveBinding.inflate(getLayoutInflater());
        View view=binding.getRoot();
        setContentView(view);

        Resources res = getResources();
        filMap(res);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            selectedNote = extras.getString("note");
        }
        //radioGroup = findViewById(R.id.radioGroup);
        //Button buttonConvert = findViewById(R.id.button_convert);

        binding.buttonConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int selectedID = binding.radioGroup.getCheckedRadioButtonId();
                radioButton = findViewById(selectedID);
                selectedOctave = radioButton.getText().toString();

                try {
                    n = findN(selectedOctave, selectedNote);

                    Double root = Math.pow(2, 0.08333);
                    Double square = Math.pow(root, n-46);
                    double frequency = Math.round(square * 440 * 100.0) / 100.0;
                    Intent intent = new Intent(OctaveActivity.this, ResultActivity.class);
                    intent.putExtra("result", frequency);
                    intent.putExtra("note", selectedNote);
                    intent.putExtra("octave", selectedOctave);
                    startActivity(intent);
                }catch (NullPointerException ex){
                    Log.d("EX map", "Value map is NULL." + ex.getMessage());

                    Toast.makeText(OctaveActivity.this, "Note " + selectedNote +
                            " doesn't exist in the required octave", Toast.LENGTH_LONG).show();
                    Toast.makeText(OctaveActivity.this, "Please select another octave",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void filMap(Resources res) {
        mapOfNotes = Map.ofEntries(
                new AbstractMap.SimpleEntry<String, Integer>("C", 1),
                new AbstractMap.SimpleEntry<String, Integer>("C#/Db", 2),
                new AbstractMap.SimpleEntry<String, Integer>("D", 3),
                new AbstractMap.SimpleEntry<String, Integer>("D#/Eb", 4),
                new AbstractMap.SimpleEntry<String, Integer>("E", 5),
                new AbstractMap.SimpleEntry<String, Integer>("F", 6),
                new AbstractMap.SimpleEntry<String, Integer>("F#/Gb", 7),
                new AbstractMap.SimpleEntry<String, Integer>("G", 8),
                new AbstractMap.SimpleEntry<String, Integer>("G#/Ab", 9),
                new AbstractMap.SimpleEntry<String, Integer>("A", 10),
                new AbstractMap.SimpleEntry<String, Integer>("A#/Bb", 11),
                new AbstractMap.SimpleEntry<String, Integer>("B", 12)
        );
        mapOfOctaves = Map.ofEntries(
                new AbstractMap.SimpleEntry<String, Integer>(res.getString(R.string.octave_contra), 0),
                new AbstractMap.SimpleEntry<String, Integer>(res.getString(R.string.octave_great), 1),
                new AbstractMap.SimpleEntry<String, Integer>(res.getString(R.string.octave_small), 2),
                new AbstractMap.SimpleEntry<String, Integer>(res.getString(R.string.first), 3),
                new AbstractMap.SimpleEntry<String, Integer>(res.getString(R.string.second), 4),
                new AbstractMap.SimpleEntry<String, Integer>(res.getString(R.string.third), 5),
                new AbstractMap.SimpleEntry<String, Integer>(res.getString(R.string.fourth), 6)
        );
        mapForSpecialCase = Map.ofEntries(
                new AbstractMap.SimpleEntry<>("A", -2),
                new AbstractMap.SimpleEntry<>("A#/Bb", -1),
                new AbstractMap.SimpleEntry<>("B", 0)
        );
    }

    private Integer findN(String selectedOctave, String selectedNote) {
        Integer note;
        Integer octave;

        try {
            note = mapOfNotes.get(selectedNote);
            octave = mapOfOctaves.get(selectedOctave);
            n = note + (octave * 12);
        } catch (NullPointerException ex) {
            Log.d("EX", "Value is NULL." + ex.getMessage());
                n = mapForSpecialCase.get(selectedNote);
        }
        return n;
    }

}