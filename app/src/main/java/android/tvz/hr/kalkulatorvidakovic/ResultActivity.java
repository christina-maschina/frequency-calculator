package android.tvz.hr.kalkulatorvidakovic;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.tvz.hr.kalkulatorvidakovic.databinding.ActivityResultBinding;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {
    private ActivityResultBinding binding;
    private PlaySound play;
    private AudioManager audioManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("IN", "I'm in result activity");

        super.onCreate(savedInstanceState);
        binding = ActivityResultBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Double frequency = extras.getDouble("result");
            String note = extras.getString("note");
            String octave = extras.getString("octave");
            //TextView textView1 = findViewById(R.id.textView1);
            //TextView textView2 = findViewById(R.id.textView2);
            binding.textView1.setText(note + " " + octave + ":");
            binding.textView2.setText(frequency.toString() + " " + getResources().getString(R.string.hz));
            play=new PlaySound(frequency);
        }

        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        initControls();
    }

    private void initControls() {
        try {
            audioManager= (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            binding.sb.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
            binding.sb.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
            binding.sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
        }catch (Exception e){

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        play.onResume();

    }

    public void backToNoteActivity(View view) {
        Intent intent = new Intent(ResultActivity.this, NoteActivity.class);
        startActivity(intent);
    }

}